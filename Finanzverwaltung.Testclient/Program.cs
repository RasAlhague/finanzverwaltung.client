﻿using System;

namespace Finanzverwaltung.Testclient
{
    static class Program
    {
        static void Main(string method, string path, bool all, int id)
        {
            Console.WriteLine("TestClient V.0.0.1\n");

            TestClient testClient = new TestClient();
            testClient.SendMessage += Console.WriteLine;
            testClient.SendFailMessage += DisplayFailure;

            if (all)
            {
                testClient.RunAll();
            }
            else
            {
                Console.WriteLine($"Try running test with method \"{method}\" and path \"{path}\"");
                testClient.Run(method, path, all, id);
            }

            Console.WriteLine("Finished tests...");
        }

        private static void DisplayFailure(string message)
        {
            ConsoleColor startColor = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ForegroundColor = startColor;
        }
    }
}
