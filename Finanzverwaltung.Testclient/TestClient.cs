﻿using Finanzverwaltung.Lib;
using Finanzverwaltung.Lib.Models;
using Options.Extensions;
using System;

namespace Finanzverwaltung.Testclient
{
    /// <summary>
    /// Client class for testing the webapi.
    /// </summary>
    public class TestClient
    {
        /// <summary>
        /// Event for logging normal output messages.
        /// </summary>
        public event Action<string> SendMessage;
        /// <summary>
        /// Event for logging failure output messages.
        /// </summary>
        public event Action<string> SendFailMessage;

        /// <summary>
        /// Runs all tests.
        /// </summary>
        public void RunAll()
        {
            using WebApiClient webApiClient = new WebApiClient("http://192.168.178.31:8000");
            SendMessage?.Invoke("Executing all tests...\n");
            SendMessage?.Invoke("Executing tests for transactions...");

            int id = PostTransaction(webApiClient);
            GetTransactions(webApiClient);
            PutTransaction(webApiClient, id);
            GetTransaction(webApiClient, id);
            DeleteTransaction(webApiClient, id);

            SendMessage?.Invoke("Executing tests for standing orders...");

            id = PostStandingOrder(webApiClient);
            GetStandingOrders(webApiClient);
            PutStandingOrder(webApiClient, id);
            GetStandingOrder(webApiClient, id);
            DeleteStandingOrder(webApiClient, id);

            SendMessage?.Invoke("Finished executing of tests...");
        }

        /// <summary>
        /// Runs the tests.
        /// </summary>
        /// <param name="method">http method to test.</param>
        /// <param name="path">path to test</param>
        /// <param name="all">Executes all tests</param>
        /// <param name="id">Id for testing</param>
        public void Run(string method, string path, bool all, int id)
        {
            using WebApiClient webApiClient = new WebApiClient("http://192.168.178.31:8000");

            if (path.ToLower() == "transaction" || path.ToLower() == "t")
            {
                switch (method.ToLower())
                {
                    case "gets":
                        GetTransactions(webApiClient);
                        break;
                    case "get":
                        GetTransaction(webApiClient, id);
                        break;
                    case "put":
                        PutTransaction(webApiClient, id);
                        break;
                    case "post":
                        PostTransaction(webApiClient);
                        break;
                    case "delete":
                        DeleteTransaction(webApiClient, id);
                        break;
                    default:
                        SendFailMessage?.Invoke($"Invalid operation, method {method} is not defined!");
                        break;
                }
            }
            else if (path.ToLower() == "standing_orders" || path.ToLower() == "s")
            {
                switch (method.ToLower())
                {
                    case "gets":
                        GetStandingOrders(webApiClient);
                        break;
                    case "get":
                        GetStandingOrder(webApiClient, id);
                        break;
                    case "put":
                        PutStandingOrder(webApiClient, id);
                        break;
                    case "post":
                        PostStandingOrder(webApiClient);
                        break;
                    case "delete":
                        DeleteStandingOrder(webApiClient, id);
                        break;
                    default:
                        SendFailMessage?.Invoke($"Invalid operation, method {method} is not defined!");
                        break;
                }
            }
            else
            {
                SendFailMessage?.Invoke($"Invalid operation, method {method} is not defined!");
            }
        }

        private void DeleteStandingOrder(WebApiClient webApiClient, int id)
        {
            SendMessage?.Invoke("\nTest deleting of standing orders...");
            var statusCode = webApiClient.DeleteStandingOrder(id);

            if (statusCode.IsSuccessState())
            {
                SendMessage?.Invoke("Successfully deleted standing order...");
            }
            else
            {
                SendFailMessage?.Invoke($"Deleting was not succesfull, http state: {statusCode}...");
            }
        }

        private int PostStandingOrder(WebApiClient webApiClient)
        {
            SendMessage?.Invoke("\nTest posting of standing orders...");

            StandingOrder standingOrder = new StandingOrder()
            {
                OrderName = "Gehalt",
                CreateDate = DateTime.Now,
                EndDate = null,
                StartDate = DateTime.MinValue,
                TransactionId = 1
            };

            var result = webApiClient.PostStandingOrder(standingOrder);

            if (result.IsSuccessCode())
            {
                SendMessage?.Invoke("Successfully posted standing order...");
                SendMessage?.Invoke($"Data received:\n{result.Value.Unwrap()}");

                return result.Value.Unwrap();
            }
            else
            {
                SendFailMessage?.Invoke($"Posting was not succesfull, http state: {result.StatusCode}...");

                return 0;
            }
        }

        private void PutStandingOrder(WebApiClient webApiClient, int id)
        {
            SendMessage?.Invoke("\nTest putting of standing orders...");

            StandingOrder standingOrder = new StandingOrder()
            {
                Id = id,
                OrderName = "Gehalt",
                CreateDate = DateTime.Now,
                EndDate = null,
                StartDate = DateTime.MinValue,
                TransactionId = 1
            };

            var result = webApiClient.PutStandingOrder(standingOrder);

            if (result.IsSuccessCode())
            {
                SendMessage?.Invoke("Successfully putted standing order...");
                SendMessage?.Invoke($"Data received:\n{result.Value.Unwrap()}");
            }
            else
            {
                SendFailMessage?.Invoke($"Putting was not succesfull, http state: {result.StatusCode}...");
            }
        }

        private void GetStandingOrder(WebApiClient webApiClient, int id)
        {
            SendMessage?.Invoke("\nTest getting of one standing orders...");

            var result = webApiClient.GetStandingOrder(id);

            if (result.IsSuccessCode())
            {
                SendMessage?.Invoke("Successfully got standing order...");
                SendMessage?.Invoke($"Data received:\n{result.Value.Unwrap().ToCsv()}");
            }
            else
            {
                SendFailMessage?.Invoke($"Getting was not succesfull, http state: {result.StatusCode}...");
            }
        }

        private void GetStandingOrders(WebApiClient webApiClient)
        {
            SendMessage?.Invoke("\nTest getting of standing orders...");

            var result = webApiClient.GetStandingOrders();

            if (result.IsSuccessCode())
            {
                SendMessage?.Invoke("Successfully got standing orders...");
                SendMessage?.Invoke($"Data received:");

                foreach (var t in result.Value.Unwrap())
                {
                    SendMessage?.Invoke(t.ToCsv());
                }
            }
            else
            {
                SendFailMessage?.Invoke($"Getting was not succesfull, http state: {result.StatusCode}...");
            }
        }

        private void DeleteTransaction(WebApiClient webApiClient, int id)
        {
            SendMessage?.Invoke("\nTest deleting of transactions...");
            var statusCode = webApiClient.DeleteTransaction(id);

            if (statusCode.IsSuccessState())
            {
                SendMessage?.Invoke("Successfully deleted transaction...");
            }
            else
            {
                SendFailMessage?.Invoke($"Deleting was not succesfull, http state: {statusCode}...");
            }
        }

        private int PostTransaction(WebApiClient webApiClient)
        {
            SendMessage?.Invoke("\nTest posting of transactions...");

            Transaction transaction = new Transaction
            {
                TransactionName = "Burrito",
                Origin = "Burrito Bay Area",
                Reason = "Hunger",
                CreateDate = DateTime.MinValue,
                EffectiveDate = DateTime.MinValue,
                Amount = 7.75,
                TypeId = 2
            };

            var result = webApiClient.PostTransaction(transaction);

            if (result.IsSuccessCode())
            {
                SendMessage?.Invoke("Successfully posted transaction...");
                SendMessage?.Invoke($"Data received:\n{result.Value.Unwrap()}");
                return result.Value.Unwrap();
            }
            else
            {
                SendFailMessage?.Invoke($"Posting was not succesfull, http state: {result.StatusCode}...");
                return 0;
            }
        }

        private void PutTransaction(WebApiClient webApiClient, int id)
        {
            SendMessage?.Invoke("\nTest putting of transactions...");

            Transaction transaction = new Transaction
            {
                Id = id,
                TransactionName = "Burrito",
                Origin = "Burrito Bay Area",
                Reason = "Ich war hungrig",
                CreateDate = DateTime.MinValue,
                EffectiveDate = DateTime.Now,
                Amount = 7.75,
                TypeId = 2
            };

            var result = webApiClient.PutTransaction(transaction);

            if (result.IsSuccessCode())
            {
                SendMessage?.Invoke("Successfully putted transaction...");
                SendMessage?.Invoke($"Data received:\n{result.Value.Unwrap().ToCsv()}");
            }
            else
            {
                SendFailMessage?.Invoke($"Putting was not succesfull, http state: {result.StatusCode}...");
            }
        }

        private void GetTransaction(WebApiClient webApiClient, int id)
        {
            SendMessage?.Invoke("\nTest getting of one transaction...");

            var result = webApiClient.GetTransaction(id);

            if (result.IsSuccessCode())
            {
                SendMessage?.Invoke("Successfully got one transaction...");
                SendMessage?.Invoke($"Data received:\n{result.Value.Unwrap().ToCsv()}");
            }
            else
            {
                SendFailMessage?.Invoke($"Getting was not succesfull, http state: {result.StatusCode}...");
            }
        }

        private void GetTransactions(WebApiClient webApiClient)
        {
            SendMessage?.Invoke("\nTest getting of transactions...");

            var result = webApiClient.GetTransactions();

            if (result.IsSuccessCode())
            {
                SendMessage?.Invoke("Successfully got transactions...");
                SendMessage?.Invoke($"Data received:");

                foreach (var t in result.Value.Unwrap())
                {
                    SendMessage?.Invoke(t.ToCsv());
                }
            }
            else
            {
                SendFailMessage?.Invoke($"Getting was not succesfull, http state: {result.StatusCode}...");
            }
        }
    }
}
