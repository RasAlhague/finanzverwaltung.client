﻿using System.Net;

namespace Finanzverwaltung.Lib
{
    public static class HttpClientExtensions
    {
        public static bool IsSuccessState(this HttpStatusCode statusCode)
        {
            return statusCode == HttpStatusCode.OK ||
                   statusCode == HttpStatusCode.Created ||
                   statusCode == HttpStatusCode.Accepted ||
                   statusCode == HttpStatusCode.NonAuthoritativeInformation ||
                   statusCode == HttpStatusCode.NoContent ||
                   statusCode == HttpStatusCode.ResetContent ||
                   statusCode == HttpStatusCode.PartialContent ||
                   statusCode == HttpStatusCode.MultipleChoices;
        }
    }
}
