﻿using Options;
using System.Net;

namespace Finanzverwaltung.Lib
{
    public class ApiResult<T>
    {
        public Option<T> Value { get; }
        public HttpStatusCode StatusCode { get; }

        public ApiResult(Option<T> value, HttpStatusCode statusCode)
        {
            Value = value;
            StatusCode = statusCode;
        }

        public bool IsSuccessCode()
        {
            return StatusCode.IsSuccessState();
        }
    }
}
