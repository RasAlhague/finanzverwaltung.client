﻿using Finanzverwaltung.Lib.Models;
using Newtonsoft.Json;
using Options;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;

namespace Finanzverwaltung.Lib
{
    public sealed class WebApiClient : IDisposable
    {
        private readonly HttpClient _httpClient;

        public WebApiClient(string baseAddress) : this(new Uri(baseAddress))
        {
        }

        public WebApiClient(Uri baseAddress)
        {
            _httpClient = new HttpClient
            {
                BaseAddress = baseAddress
            };
        }

        public ApiResult<IEnumerable<Transaction>> GetTransactions()
        {
            HttpResponseMessage resp = _httpClient.GetAsync("/api/transaction").Result;

            if (resp.IsSuccessStatusCode)
            {
                string content = resp.Content.ReadAsStringAsync().Result;
                IEnumerable<Transaction> transactions = JsonConvert.DeserializeObject<IEnumerable<Transaction>>(content);

                return new ApiResult<IEnumerable<Transaction>>(new Some<IEnumerable<Transaction>>(transactions), resp.StatusCode);
            }
            else
            {
                return new ApiResult<IEnumerable<Transaction>>(None.Value, resp.StatusCode);
            }
        }

        public ApiResult<Transaction> GetTransaction(int id)
        {
            HttpResponseMessage resp = _httpClient.GetAsync($"/api/transaction/{id}").Result;

            if (resp.IsSuccessStatusCode)
            {
                string content = resp.Content.ReadAsStringAsync().Result;

                Transaction transactions = JsonConvert.DeserializeObject<Transaction>(content);

                return new ApiResult<Transaction>(transactions, resp.StatusCode);
            }
            else
            {
                return new ApiResult<Transaction>(None.Value, resp.StatusCode);
            }
        }

        public ApiResult<Transaction> PutTransaction(Transaction transaction)
        {
            string json = JsonConvert.SerializeObject(transaction);
            StringContent data = new StringContent(json, Encoding.Default, "application/json");

            HttpResponseMessage resp = _httpClient.PutAsync($"/api/transaction/{transaction.Id}", data).Result;

            if (resp.IsSuccessStatusCode)
            {
                string content = resp.Content.ReadAsStringAsync().Result;

                Transaction transactions = JsonConvert.DeserializeObject<Transaction>(content);

                return new ApiResult<Transaction>(transactions, resp.StatusCode);
            }
            else
            {
                return new ApiResult<Transaction>(None.Value, resp.StatusCode);
            }
        }

        public ApiResult<int> PostTransaction(Transaction transaction)
        {
            string json = JsonConvert.SerializeObject(transaction);
            StringContent data = new StringContent(json, Encoding.Default, "application/json");

            HttpResponseMessage resp = _httpClient.PostAsync("/api/transaction", data).Result;

            if (resp.IsSuccessStatusCode)
            {
                string content = resp.Content.ReadAsStringAsync().Result;

                int id = JsonConvert.DeserializeObject<int>(content);
                return new ApiResult<int>(id, resp.StatusCode);
            }
            else
            {
                return new ApiResult<int>(None.Value, resp.StatusCode);
            }
        }

        public HttpStatusCode DeleteTransaction(int id)
        {
            HttpResponseMessage resp = _httpClient.DeleteAsync($"/api/transaction/{id}").Result;

            return resp.StatusCode;
        }

        public ApiResult<IEnumerable<StandingOrder>> GetStandingOrders()
        {
            HttpResponseMessage resp = _httpClient.GetAsync("/api/standing_order").Result;

            if (resp.IsSuccessStatusCode)
            {
                string content = resp.Content.ReadAsStringAsync().Result;

                IEnumerable<StandingOrder> ret = JsonConvert.DeserializeObject<IEnumerable<StandingOrder>>(content);

                return new ApiResult<IEnumerable<StandingOrder>>(new Some<IEnumerable<StandingOrder>>(ret), resp.StatusCode);
            }
            else
            {
                return new ApiResult<IEnumerable<StandingOrder>>(None.Value, resp.StatusCode);
            }
        }

        public ApiResult<StandingOrder> GetStandingOrder(int id)
        {
            HttpResponseMessage resp = _httpClient.GetAsync($"/api/standing_order/{id}").Result;

            if (resp.IsSuccessStatusCode)
            {
                string content = resp.Content.ReadAsStringAsync().Result;

            StandingOrder ret = JsonConvert.DeserializeObject<StandingOrder>(content);

            return new ApiResult<StandingOrder>(ret, resp.StatusCode);
            }
            else
            {
                return new ApiResult<StandingOrder>(None.Value, resp.StatusCode);
            }
        }

        public ApiResult<StandingOrder> PutStandingOrder(StandingOrder standingOrder)
        {
            string json = JsonConvert.SerializeObject(standingOrder);
            StringContent data = new StringContent(json, Encoding.Default, "application/json");

            HttpResponseMessage resp = _httpClient.PutAsync($"/api/standing_order/{standingOrder.Id}", data).Result;

                if (resp.IsSuccessStatusCode)
                {
                    string content = resp.Content.ReadAsStringAsync().Result;

            StandingOrder ret = JsonConvert.DeserializeObject<StandingOrder>(content);

            return new ApiResult<StandingOrder>(ret, resp.StatusCode);
            }
            else
            {
                return new ApiResult<StandingOrder>(None.Value, resp.StatusCode);
            }
        }

        public ApiResult<int> PostStandingOrder(StandingOrder standingOrder)
        {
            string json = JsonConvert.SerializeObject(standingOrder);
            StringContent data = new StringContent(json, Encoding.Default, "application/json");

            HttpResponseMessage resp = _httpClient.PostAsync("/api/standing_order", data).Result;

                    if (resp.IsSuccessStatusCode)
                    {
                        string content = resp.Content.ReadAsStringAsync().Result;

            int id = JsonConvert.DeserializeObject<int>(content);

                return new ApiResult<int>(id, resp.StatusCode);
            }
            else
            {
                return new ApiResult<int>(None.Value, resp.StatusCode);
            }
        }

        public HttpStatusCode DeleteStandingOrder(int id)
        {
            HttpResponseMessage resp = _httpClient.DeleteAsync($"/api/standing_order/{id}").Result;

            return resp.StatusCode;
        }

        public void Dispose()
        {
            _httpClient.Dispose();
        }
    }
}
