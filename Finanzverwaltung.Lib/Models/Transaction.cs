﻿using Newtonsoft.Json;
using System;

namespace Finanzverwaltung.Lib.Models
{
    public class Transaction
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("transaction_name")]
        public string TransactionName { get; set; }
        [JsonProperty("origin")]
        public string Origin { get; set; }
        [JsonProperty("reason")]
        public string Reason { get; set; }
        [JsonConverter(typeof(CustomDateTimeConverter))]
        [JsonProperty("create_date")]
        public DateTime CreateDate { get; set; }
        [JsonConverter(typeof(CustomDateTimeConverter))]
        [JsonProperty("effective_date")]
        public DateTime EffectiveDate { get; set; }
        [JsonProperty("amount")]
        public double Amount { get; set; }
        [JsonProperty("type_id")]
        public int TypeId { get; set; }

        public string ToCsv()
        {
            return $@"{Id}; {TransactionName}; {Origin}; {Reason}; {CreateDate}; {EffectiveDate}; {Amount}; {TypeId}";
        }
    }
}
