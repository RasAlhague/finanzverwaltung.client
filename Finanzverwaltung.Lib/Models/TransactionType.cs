﻿using Newtonsoft.Json;

namespace Finanzverwaltung.Lib.Models
{
    public class TransactionType
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("type_name")]
        public string TypeName { get; set; }

        public string ToCsv()
        {
            return $@"{Id}; {TypeName}";
        }
    }
}
