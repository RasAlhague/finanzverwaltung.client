﻿using Newtonsoft.Json;
using System;

namespace Finanzverwaltung.Lib.Models
{
    public class StandingOrder
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("order_name")]
        public string OrderName { get; set; }
        [JsonConverter(typeof(CustomDateTimeConverter))]
        [JsonProperty("start_date")]
        public DateTime StartDate { get; set; }
        [JsonConverter(typeof(CustomDateTimeConverter))]
        [JsonProperty("end_date")]
        public DateTime? EndDate { get; set; }
        [JsonConverter(typeof(CustomDateTimeConverter))]
        [JsonProperty("create_date")]
        public DateTime CreateDate { get; set; }
        [JsonProperty("transaction_id")]
        public int TransactionId { get; set; }

        public string ToCsv()
        {
            return $@"{Id}; {OrderName}; {StartDate}; {EndDate}; {CreateDate}; {CreateDate}; {TransactionId}";
        }
    }
}
